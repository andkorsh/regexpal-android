package com.andkorsh.regexpal.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import android.util.Pair;

public class RegexManager
{

    private static RegexManager instance = null;

    private RegexManager()
    {
    }

    public static RegexManager getInstance()
    {
        if (instance == null)
        {
            instance = new RegexManager();
        }

        return instance;
    }

    public List<Pair<Integer, Integer>> getAllOccurrences(String regex,
            String data, boolean caseInsensitive, boolean dotMathesAll,
            boolean multiline) throws PatternSyntaxException
    {
        List<Pair<Integer, Integer>> occurrencies = new ArrayList<Pair<Integer, Integer>>();

        int flags = 0;

        if (caseInsensitive)
        {
            flags |= Pattern.CASE_INSENSITIVE;
        }
        if (dotMathesAll)
        {
            flags |= Pattern.DOTALL;
        }
        if (multiline)
        {
            flags |= Pattern.MULTILINE;
        }

        Matcher matcher = Pattern.compile(regex, flags).matcher(data);

        while (matcher.find())
        {
            occurrencies.add(Pair.create(matcher.start(), matcher.end()));
        }

        return occurrencies;
    }
}
