package com.andkorsh.regexpal.domain;

public class Constants
{
    public static final String DATABASE_NAME = "com.andkorsh.regex";
    
    public static final int CURRENT_DATABASE_VERSION = 29;
}
