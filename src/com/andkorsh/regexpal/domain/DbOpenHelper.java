package com.andkorsh.regexpal.domain;

import com.andkorsh.regexpal.BuildConfig;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final public class DbOpenHelper extends SQLiteOpenHelper
{
    private static final String CREATE_TABLE_QUERY      = "create table regexes (_id integer primary key autoincrement, alias text, regex text, data text, flags integer, modified integer)";

    private static final String GET_REGEX_BY_ID_QUERY_1 = "select * from regexes where _id = %d";

    private static final String GET_ALL_REGEXES         = "select * from regexes order by modified desc";

    private static final String GET_COUNT               = "select count(*) from regexes";
    
    private static final String INSERT_REGEX_QUERY      = "insert into regexes (alias, regex, data, flags, modified) values ('%s', '%s', '%s', '%d', %d)";
    
    private static final String DROP_TABLE              = "drop table regexes"; 
    
    private static final String DELETE_REGEX_QUERY_1    = "delete from regexes where _id = %d";
    
    private static final int ID_COLUMN_INDEX = 0;
    private static final int ALIAS_COLUMN_INDEX = 1;
    private static final int REGEX_COLUMN_INDEX = 2;
    private static final int DATA_COLUMN_INDEX = 3;
    private static final int FLAGS_COLUMN_INDEX = 4;
    
    private SQLiteDatabase database;

    private static DbOpenHelper instance = null;
    
    private DbOpenHelper(Context context)
    {
        super(context, Constants.DATABASE_NAME, null,
                Constants.CURRENT_DATABASE_VERSION);
        
        database = getWritableDatabase();
    }
    
    public static DbOpenHelper getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new DbOpenHelper(context);
        }
        
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {
        database.execSQL(CREATE_TABLE_QUERY);

        if (BuildConfig.DEBUG)
        {
            database.execSQL(String.format(INSERT_REGEX_QUERY, "My love regex", "regex 1", "data 1", 0x000, System.currentTimeMillis()));
            database.execSQL(String.format(INSERT_REGEX_QUERY, "Email validator", "[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})", "Hello, my name is John. My work email is john.doe@support.example.org, and my personal email is john@gmail.com. Email me.", 0xF00, System.currentTimeMillis()));
            database.execSQL(String.format(INSERT_REGEX_QUERY, "Phone number", "regex 3", "data 3", 0x00F, System.currentTimeMillis()));
        }
    }
    
    @Override
    public void onUpgrade(SQLiteDatabase database, int i, int i1)
    {
        database.execSQL(DROP_TABLE);
        onCreate(database);
    }

    public RegexEntity getRegex(int id)
    {
        Cursor cursor = database.rawQuery(String.format(GET_REGEX_BY_ID_QUERY_1, id), null);
        
        cursor.moveToFirst();
        
        RegexEntity res = new RegexEntity(id, cursor.getString(ALIAS_COLUMN_INDEX),
                cursor.getString(REGEX_COLUMN_INDEX), cursor.getString(DATA_COLUMN_INDEX), cursor.getInt(FLAGS_COLUMN_INDEX));

        cursor.close();
        
        return res;
    }

    public int getCount()
    {
        Cursor cursor = database.rawQuery(GET_COUNT, null);
        cursor.moveToFirst();
        int res = cursor.getInt(0);
        cursor.close();
        return res;
    }

    public RegexEntity[] getAllRegexes()
    {
        RegexEntity[] result = new RegexEntity[getCount()];

        Cursor cursor = database.rawQuery(GET_ALL_REGEXES, null);
        
        int arrayPos = 0;
        while (cursor.moveToNext())
        {
            result[arrayPos++] = new RegexEntity(cursor.getInt(ID_COLUMN_INDEX),
                    cursor.getString(ALIAS_COLUMN_INDEX),
                    cursor.getString(REGEX_COLUMN_INDEX),
                    cursor.getString(DATA_COLUMN_INDEX),
                    cursor.getInt(FLAGS_COLUMN_INDEX));
        }
        
        cursor.close();
        
        return result;
    }
    
    public boolean updateRegex(RegexEntity r)
    {
        String strFilter = "_id=" + r.getId();
        
        ContentValues args = new ContentValues();
        args.put("alias", r.getAlias());
        args.put("regex", r.getRegex());
        args.put("data", r.getData());
        args.put("flags", r.getFlags());
        args.put("modified", System.currentTimeMillis());
        
        return database.update("regexes", args, strFilter, null) == 1;
    }
    
    public int insertRegex(RegexEntity r)
    {
        ContentValues args = new ContentValues();
        args.put("alias", r.getAlias());
        args.put("regex", r.getRegex());
        args.put("data", r.getData());
        args.put("flags", r.getFlags());
        args.put("modified", System.currentTimeMillis());
        
        return (int) database.insert("regexes", null, args);
    }
    
    public void deleteRegex(int regexId)
    {
        database.execSQL(String.format(DELETE_REGEX_QUERY_1, regexId));
    }
    
    @Override
    public void finalize()
    {
        database.close();
    }
}
