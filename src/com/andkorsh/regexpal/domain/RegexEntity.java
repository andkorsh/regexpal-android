package com.andkorsh.regexpal.domain;

import java.util.HashMap;

public class RegexEntity extends HashMap<String, String>
{
    private static final long serialVersionUID = -7830179696696316164L;

    private int id;
    private String alias;
    private String regex;
    private String data;
    private int flags;
    
    public static final String KEY_ID = "id";
    public static final String KEY_ALIAS = "alias";
    public static final String KEY_REGEX = "regex";
    public static final String KEY_DATA = "data";

    public RegexEntity(int id, String alias, String regex, String data, int flags)
    {
        this.id = id;
        this.alias = alias;
        this.regex = regex;
        this.flags = flags;
        this.data = data;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getAlias()
    {
        return alias;
    }

    public void setAlias(String alias)
    {
        this.alias = alias;
    }

    public String getRegex()
    {
        return regex;
    }
    
    public String getData()
    {
        return data;
    }

    public void setData(String data)
    {
        this.data = data;
    }
    
    public void setRegex(String regex)
    {
        this.regex = regex;
    }
    
    public boolean getCaseInsensitive()
    {
        return (flags & 0x00F) != 0;
    }
    
    public boolean getDotMathesAll()
    {
        return (flags & 0x0F0) != 0;
    }
    
    public boolean getMultiline()
    {
        return (flags & 0xF00) != 0;
    }
    
    public void setFlags(int flags)
    {
        this.flags = flags;
    }
    
    public int getFlags()
    {
        return flags;
    }
    
    public void setCaseInsensitive(boolean caseInsensitive)
    {
        if (caseInsensitive)
        {
            flags |= 0x00F;
        }
        else
        {
            flags &= 0xFF0;
        }
    }
    
    public void setDotMathesAll(boolean dotMathesAll)
    {
        if (dotMathesAll)
        {
            flags |= 0x0F0;
        }
        else
        {
            flags &= 0xF0F;
        }
    }
    
    public void setMultiline(boolean multiline)
    {
        if (multiline)
        {
            flags |= 0xF00;
        }
        else
        {
            flags &= 0x0FF;
        }
    }
    
    @Override
    public String get(Object key)
    {
        String key_s = (String) key;
        
        if (KEY_ID.equals(key_s))
        {
            return Integer.toString(id);
        }
        else if (KEY_ALIAS.equals(key_s))
        {
            return alias;
        }
        else if (KEY_REGEX.equals(key_s))
        {
            return regex;
        }
        else if (KEY_DATA.equals(key_s))
        {
            return data;
        }
        else
        {
            return null;
        }
    }
}
