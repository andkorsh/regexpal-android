package com.andkorsh.regexpal.helpers;

import com.stevenlevithan.RegexColorizer;
import com.stevenlevithan.regex_colorizer;

public class RegexHighlighter
{
    private static RegexHighlighter instance = null;
    private RegexColorizer regexColorizer;

    private RegexHighlighter()
    {
        regexColorizer = new regex_colorizer();
    }

    public static RegexHighlighter getInstance()
    {
        // Temporary block singleton because its lag on old devices.
        //if (instance == null)
        if (true)
        {
            instance = new RegexHighlighter();
        }

        return instance;
    }

    public String highlight(String regex)
    {
        String colorizedText = regexColorizer.colorizeText(regex);
        String styledHtml = applyStyle(colorizedText);
        return styledHtml;
    }

    private String applyStyle(String html)
    {
        html = html.replaceAll("<b>", "<font color=\"#0707ee\">");
        html = html.replaceAll("<i>", "<font color=\"#1db000\">");
        html = html.replaceAll("<u>", "<font color=\"#007a09\">");
        html = html.replaceAll("<b class=\"g1\">", "<font color=\"#ec9b00\">");
        html = html.replaceAll("<b class=\"g2\">", "<font color=\"#9d6400\">");
        html = html.replaceAll("<b class=\"g3\">", "<font color=\"#c24699\">");
        html = html.replaceAll("<b class=\"g4\">", "<font color=\"#00909d\">");
        html = html.replaceAll("<b class=\"g5\">", "<font color=\"#81009d\">");
        html = html.replaceAll("<b class=\"err\".*?>",
                "<font color=\"#d50000\">");
        html = html.replaceAll("</b>", "</font>");
        html = html.replaceAll("</i>", "</font>");
        html = html.replaceAll("</u>", "</font>");

        return html;
    }
}
