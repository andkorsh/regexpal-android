package com.andkorsh.regexpal.helpers;

import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.CharacterStyle;

public class SpansHelper
{
    public static void fromHtml(String html, Spannable out)
    {
        removeSpans(out, CharacterStyle.class);

        Spanned result = Html.fromHtml(html);
        Object[] spansres = result.getSpans(0, result.length(), Object.class);
        
        if (spansres != null && spansres.length > 0)
        {
            for (int i = spansres.length - 1; i >= 0; --i)
            {
                out.setSpan(spansres[i], result.getSpanStart(spansres[i]),
                        result.getSpanEnd(spansres[i]),
                        result.getSpanFlags(spansres[i]));
            }
        }
    }

    public static void removeSpans(Spannable spannable, Class<?> cls)
    {
        Object[] oldSpans = spannable.getSpans(0, spannable.length(), cls);
        for (Object span : oldSpans)
        {
            spannable.removeSpan(span);
        }
    }
}
