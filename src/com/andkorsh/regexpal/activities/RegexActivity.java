package com.andkorsh.regexpal.activities;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.regex.PatternSyntaxException;

import com.andkorsh.regexpal.R;
import com.andkorsh.regexpal.domain.DbOpenHelper;
import com.andkorsh.regexpal.domain.RegexEntity;
import com.andkorsh.regexpal.domain.RegexManager;
import com.andkorsh.regexpal.helpers.SpansHelper;
import com.andkorsh.regexpal.helpers.UrlParser;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RegexActivity extends Activity
{
    public static final String REGEX_ID_EXTRA_KEY = "regexId";
    
    private EditText regexField;
    private EditText dataField;
    private TextView validationLabel;
    private TextView legalLabel;
    private Menu menu;
    private SlidingMenu referencesMenu;
    
    private static RegexManager regexManager;
    private static int[] highlightColors;
    private static SettingsHelper settingsHelper;
    private static RegexEntity regexEntity;
    private static boolean isChanged;
    private static DbOpenHelper dbHelper;
    private static RegexFieldHighlightTask regexFieldHighlightTask;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regex_activity);
        
        // Configure the SlidingMenu see https://github.com/jfeinstein10/SlidingMenu
        referencesMenu = new SlidingMenu(this);
        referencesMenu.setMode(SlidingMenu.RIGHT);
        referencesMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        referencesMenu.setShadowWidthRes(R.dimen.regex_referencesMenu_shadow_width);
        referencesMenu.setShadowDrawable(R.drawable.shadow);
        referencesMenu.setBehindOffsetRes(R.dimen.regex_referencesMenu_offset);
        referencesMenu.setFadeDegree(1);
        referencesMenu.setFadeEnabled(true);
        referencesMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        referencesMenu.setMenu(R.layout.quick_references_activity);

        ListView quickReferencesList = (ListView) findViewById(R.id.quickReferencesList);
        
        final String[] quickReferenceItems = getResources().getStringArray(R.array.quick_references_array);
        Spanned[] quickReferenceSequences = new Spanned[quickReferenceItems.length];
        
        for (int i = 0; i < quickReferenceItems.length; i++) {
        	quickReferenceSequences[i] = Html.fromHtml(quickReferenceItems[i]);
		}
        
        quickReferencesList.setAdapter(new ArrayAdapter<Spanned>(this, android.R.layout.simple_list_item_1, quickReferenceSequences)
        {        	
            @Override
            public View getView(int position, View convertView, ViewGroup parent) 
            {
                View resultRow;

                if (convertView == null) {
                	resultRow = getLayoutInflater().inflate(android.R.layout.simple_list_item_1, null);
                } else {
                	resultRow = convertView;
                }

                ((TextView) resultRow.findViewById(android.R.id.text1)).setText(getItem(position));

                return resultRow;
            }
        });
        
        regexField = (EditText) findViewById(R.id.regexField);
        dataField = (EditText) findViewById(R.id.dataField);
        validationLabel = (TextView) findViewById(R.id.regexValidationLabel);
        legalLabel = (TextView) findViewById(R.id.legalLabel);
        dbHelper = DbOpenHelper.getInstance(getApplicationContext());
        
        legalLabel.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(getString(R.string.regexActivity_legalInfoLabel_url)));
                startActivity(browserIntent);
            }
        });

        dataField.setMinLines(dataField.getLineCount());
        
        highlightColors = getResources().getIntArray(R.array.HighlightColors);
        regexFieldHighlightTask = RegexFieldHighlightTask.getInstance();
        regexManager = RegexManager.getInstance();
        
        Intent intent = getIntent();

        // Try to get the regex id from intent.
        int regexId = intent.getIntExtra(REGEX_ID_EXTRA_KEY, -2);
        String datastr = intent.getDataString();
        
        if (regexId >= 0)
        {
            regexEntity = dbHelper.getRegex(regexId);
            
            settingsHelper = new SettingsHelper(this, regexEntity.getCaseInsensitive(), 
                    regexEntity.getDotMathesAll(), regexEntity.getMultiline());
            
            regexField.setText(regexEntity.getRegex());
            regexField.setSelection(regexEntity.getRegex().length());

            dataField.setText(regexEntity.getData());
            
            regexFieldHighlightTask.highlight(regexField.getEditableText());
            highlightOccurrencies();
        }
        else if (datastr != null)
        {
            Map<String, List<String>> params = UrlParser.getQueryParams(datastr);
            
            if (params != null)
            {
                regexEntity = new RegexEntity(-1, this.getString(R.string.general_regexDefaultAlias_text), "", "", 0);
                
                if (params.containsKey(getString(R.string.general_regexpal_permalink_flagsKey)))
                {
                    String flags = params.get(getString(R.string.general_regexpal_permalink_flagsKey)).get(0);
                    
                    boolean i, m, s;
                    
                    i = flags.contains("i");
                    m = flags.contains("m");
                    s = flags.contains("s");
                    
                    settingsHelper = new SettingsHelper(this, i, s, m);
                }
                else
                {
                    settingsHelper = new SettingsHelper(this, false, false, false);                    
                }
                
                if (params.containsKey(getString(R.string.general_regexpal_permalink_regexKey)))
                {
                    String regex = params.get(getString(R.string.general_regexpal_permalink_regexKey)).get(0);
                    
                    regexField.setText(regex);
                    regexField.setSelection(regex.length());
                    regexFieldHighlightTask.highlight(regexField.getEditableText());
                }
                
                if (params.containsKey(getString(R.string.general_regexpal_permalink_dataKey)))
                {
                    String data = params.get(getString(R.string.general_regexpal_permalink_dataKey)).get(0);
                    
                    dataField.setText(data);
                    dataField.setSelection(data.length());
                    highlightOccurrencies();
                }
            }
        }
        else if (regexId == -1 || regexEntity == null)
        {
            regexEntity = new RegexEntity(-1, this.getString(R.string.general_regexDefaultAlias_text), "", "", 0);
            
            settingsHelper = new SettingsHelper(this, false, false, false);
        }
        
        regexField.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
                regexFieldHighlightTask.highlight(s);
                highlightOccurrencies();
                checkChanges();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                    int arg2, int arg3)
            {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3)
            {
            }
        });

        dataField.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void afterTextChanged(Editable s)
            {
                highlightOccurrencies();
                checkChanges();
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1,
                    int arg2, int arg3)
            {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                    int arg3)
            {
            }
        });
    }
    
    public void checkChanges()
    {
        boolean value = !(regexEntity.getRegex().equals(regexField.getText().toString())
            && regexEntity.getData().equals(dataField.getText().toString())
            && regexEntity.getCaseInsensitive() == settingsHelper.isCaseInsensitive()
            && regexEntity.getDotMathesAll() == settingsHelper.isDotMathesAll()
            && regexEntity.getMultiline() == settingsHelper.isMultiline());
        
        isChanged = value;
        
        if (menu != null)
        {
            MenuItem menuitem = menu.findItem(R.id.mainMenu_menuItem_save);
            
            if (menuitem != null)
            {
                menuitem.setVisible(value);
            }
        }
        
        String prefix = "";
        
        if (value == true)
        {
            prefix = getString(R.string.regexActivity_changedRegex_titlePrefix);
        }
        
        this.setTitle(prefix + regexEntity.getAlias() + getString(R.string.regexActivity_nameTitleSeparator) + 
                getString(R.string.general_applicationName));
    }

    public void back()
    {
        super.onBackPressed();
    }
    
    @Override
    public void onBackPressed()
    {
        if (!isChanged)
        {
            back();
            
            return;
        }
        
        AlertDialog.Builder adbuilder = new AlertDialog.Builder(this);

        adbuilder.setTitle(getString(R.string.wantToSaveDialog_title));
        adbuilder.setMessage(getString(R.string.wantToSaveDialog_messageLabel_text));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                switch (which)
                {
                    case DialogInterface.BUTTON_NEGATIVE:
                        back();
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        break;
                    case DialogInterface.BUTTON_POSITIVE:
                        save(true, false);
                        break;
                }
            }
        };
        
        adbuilder.setNegativeButton(getString(R.string.wantToSaveDialog_noButton_text), listener);
        adbuilder.setNeutralButton(getString(R.string.wantToSaveDialog_cancelButton_text), listener);
        adbuilder.setPositiveButton(getString(R.string.wantToSaveDialog_yesButton_text), listener);
        
        adbuilder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main_menu, menu);
        checkChanges();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.mainMenu_menuItem_settings:
            {
                settingsHelper.showSettingsDialog(this, new Callable<Void>()
                {
                    @Override
                    public Void call() throws Exception
                    {
                        highlightOccurrencies();
                        checkChanges();
                        return null;
                    }
                });
                return true;
            }

            case R.id.mainMenu_menuItem_save:
            {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(regexField.getWindowToken(), 0);
                save(false, false);
                return true;
            }

            case R.id.mainMenu_menuItem_saveAs:
            {
                save(false, true);
                return true;
            }
                
            case R.id.mainMenu_menuItem_rateApp:
            {
                try
                {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getString(R.string.general_applicationPackage))));
                }
                catch (Exception ex)
                {
                    String url = "https://play.google.com/store/apps/details?id=" + getString(R.string.general_applicationPackage);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
                return true;
            }
                
            case R.id.mainMenu_menuItem_buyBook:
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(getString(R.string.general_buyBook_url)));
                startActivity(browserIntent);
                return true;
            }
                
            case R.id.mainMenu_menuItem_quickReference:
            {
                if (referencesMenu != null)
                {
                    referencesMenu.toggle();
                }
                return true;
            }
                
            case R.id.mainMenu_menuItem_permalink:
            {
                copyToClipboard(generateRegexPalLink());
                Toast.makeText(getApplicationContext(), R.string.regexActivity_permalinkHasBeenCopied_toast, Toast.LENGTH_SHORT).show();
                return true;
            }
                
            case R.id.mainMenu_menuItem_openInBrowser:
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(generateRegexPalLink()));
                startActivity(browserIntent);
                return true;
            }
                
            default:
            {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void highlightOccurrencies()
    {
        try
        {
            Editable regex = regexField.getText();
            Editable data = dataField.getText();

            // Find all start-end pair of occurrences.
            List<Pair<Integer, Integer>> occurrences = regexManager
                    .getAllOccurrences(regex.toString(), data.toString(),
                            settingsHelper.isCaseInsensitive(),
                            settingsHelper.isDotMathesAll(),
                            settingsHelper.isMultiline());

            // Remove all old spans.
            SpansHelper.removeSpans(data, CharacterStyle.class);

            // Set new spans.
            int colorIndex = 0;
            for (Pair<Integer, Integer> occurrence : occurrences)
            {
                BackgroundColorSpan span = new BackgroundColorSpan(
                        highlightColors[colorIndex]);
                data.setSpan(span, occurrence.first, occurrence.second,
                        Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                if (++colorIndex == highlightColors.length)
                {
                    colorIndex = 0;
                }
            }

            clearRegexMessage();
        }
        catch (PatternSyntaxException ex)
        {
            showRegexError(ex.getDescription());
        }
    }

    private void clearRegexMessage()
    {
        validationLabel.setVisibility(View.GONE);
        validationLabel.setText("");
    }

    private void showRegexError(String message)
    {
        validationLabel.setTextColor(getResources().getColor(
                R.color.main_regexvalidationview_textcolor));
        validationLabel.setText(message);
        validationLabel.setVisibility(View.VISIBLE);
        SpansHelper.removeSpans(dataField.getText(), CharacterStyle.class);
    }
    
    private void save(final boolean exit, final boolean saveas)
    {
        regexEntity.setRegex(regexField.getText().toString());
        regexEntity.setData(dataField.getText().toString());
        regexEntity.setCaseInsensitive(settingsHelper.isCaseInsensitive());
        regexEntity.setDotMathesAll(settingsHelper.isDotMathesAll());
        regexEntity.setMultiline(settingsHelper.isMultiline());
        
        if (regexEntity.getId() >= 0 && !saveas)
        {
            if(dbHelper.updateRegex(regexEntity))
            {
                checkChanges();
                
                if (exit)
                {
                    back();
                }
            }
            else
            {
                Toast.makeText(this, getString(R.string.regexActivity_saveErrorToast_text), 
                        Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            AlertDialog.Builder adbuilder = new AlertDialog.Builder(this);

            adbuilder.setMessage(getString(R.string.saveAsDialog_messageLabel_text));
            
            final EditText input = new EditText(this);  
            
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
            input.setSingleLine(true);
            input.setLayoutParams(lp);
            input.setText(regexEntity.getAlias());
            adbuilder.setView(input);
            input.setSelection(regexEntity.getAlias().length());
            input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            adbuilder.setTitle(getString(R.string.saveAsDialog_title));

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    switch (which)
                    {
                        case DialogInterface.BUTTON_POSITIVE:
                            regexEntity.setAlias(input.getText().toString());
                            regexEntity.setId(dbHelper.insertRegex(regexEntity));
                            if (regexEntity.getId() < 0)
                            {
                                Toast.makeText(RegexActivity.this, getString(R.string.regexActivity_saveErrorToast_text), 
                                        Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                checkChanges();
                                
                                if (exit)
                                {
                                    back();
                                }
                            }
                            break;
                    }
                }
            };
            
            adbuilder.setPositiveButton(getString(R.string.saveAsDialog_okButton_text), listener);
            adbuilder.setNegativeButton(getString(R.string.saveAsDialog_cancelButton_text), null);

            adbuilder.show();
        }
    }
    
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    private void copyToClipboard(String text)
    {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB)
        {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(getString(R.string.general_applicationName), text);
            clipboard.setPrimaryClip(clip);
        }
        else
        {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            clipboard.setText(text);
        }
    }
    
    private String generateRegexPalLink()
    {
        try
        {
            String link = this.getString(R.string.general_regexpal_permalink);
            String flagsKey = this.getString(R.string.general_regexpal_permalink_flagsKey);
            String regexKey = this.getString(R.string.general_regexpal_permalink_regexKey);
            String dataKey = this.getString(R.string.general_regexpal_permalink_dataKey);
            
            String flags = "g";
            
            if (settingsHelper.isCaseInsensitive()) flags += "i";
            if (settingsHelper.isDotMathesAll()) flags += "s";
            if (settingsHelper.isMultiline()) flags += "m";
            
            link += String.format("?%s=%s", URLEncoder.encode(flagsKey, "utf-8"), URLEncoder.encode(flags, "utf-8"));
            link += String.format("&%s=%s", URLEncoder.encode(regexKey, "utf-8"), URLEncoder.encode(regexField.getText().toString(), "utf-8"));
            link += String.format("&%s=%s", URLEncoder.encode(dataKey, "utf-8"), URLEncoder.encode(dataField.getText().toString(), "utf-8"));
            
            return link;
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
