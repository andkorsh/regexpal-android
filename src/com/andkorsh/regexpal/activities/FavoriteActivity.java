package com.andkorsh.regexpal.activities;

import java.util.Arrays;
import java.util.List;

import com.andkorsh.regexpal.R;
import com.andkorsh.regexpal.domain.DbOpenHelper;
import com.andkorsh.regexpal.domain.RegexEntity;
import com.andkorsh.regexpal.helpers.RegexHighlighter;
import com.andkorsh.regexpal.helpers.SpansHelper;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils.TruncateAt;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class FavoriteActivity extends ListActivity
{
    private Context context;
    private RegexHighlighter highlighter;
    private SimpleAdapter adapter;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.favorite_activity);

        context = this;
        highlighter = RegexHighlighter.getInstance();

        adapter = getAdapter();
        
        this.setListAdapter(adapter);

        this.getListView().setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                int regexId = Integer.parseInt(((RegexEntity) parent.getAdapter()
                        .getItem(position)).get(RegexEntity.KEY_ID));

                Intent regexIntent = new Intent(context, RegexActivity.class);
                regexIntent.putExtra(RegexActivity.REGEX_ID_EXTRA_KEY, regexId);
                context.startActivity(regexIntent);
            }
        });

        this.getListView().setOnItemLongClickListener(new OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id)
            {
                final int regexId = Integer.parseInt(((RegexEntity) parent.getAdapter()
                        .getItem(position)).get(RegexEntity.KEY_ID));
                
                final RegexEntity regexEntity = DbOpenHelper.getInstance(getApplicationContext()).getRegex(regexId);
                
                AlertDialog.Builder adbuilder = new AlertDialog.Builder(FavoriteActivity.this);

                adbuilder.setTitle(regexEntity.getAlias());

                Spannable spannable = new SpannableString(regexEntity.getRegex());
                SpansHelper.fromHtml(highlighter.highlight(regexEntity.getRegex()), spannable);
                adbuilder.setMessage(spannable);

                adbuilder.setNegativeButton(getString(R.string.renameOrDeleteDiaog_deleteButtonDialog_text), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        AlertDialog.Builder adbuilder = new AlertDialog.Builder(FavoriteActivity.this);
                        adbuilder.setTitle(getString(R.string.deleteDiaog_title));
                        adbuilder.setMessage(getString(R.string.deleteDiaog_messageLabel_text));

                        adbuilder.setPositiveButton(getString(R.string.deleteDiaog_yesButtonText), new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                DbOpenHelper.getInstance(getApplicationContext()).deleteRegex(regexId);
                                
                                setListAdapter(getAdapter());
                            }
                        });
                        
                        adbuilder.setNegativeButton(getString(R.string.deleteDiaog_noButtonText), null);
                        
                        adbuilder.show();
                        
                        setListAdapter(getAdapter());
                    }
                });
                
                adbuilder.setPositiveButton(getString(R.string.renameOrDeleteDiaog_renameButtonDialog_text), new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        AlertDialog.Builder adbuilder = new AlertDialog.Builder(FavoriteActivity.this);

                        
                        final EditText input = new EditText(FavoriteActivity.this);  
                        
                        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                        input.setSingleLine(true);
                        input.setLayoutParams(lp);
                        input.setText(regexEntity.getAlias());
                        adbuilder.setView(input);
                        input.setSelection(regexEntity.getAlias().length());
                        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                        adbuilder.setTitle(getString(R.string.renameDiaog_title));

                        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which)
                            {
                                switch (which)
                                {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        regexEntity.setAlias(input.getText().toString());
                                        DbOpenHelper.getInstance(getApplicationContext()).updateRegex(regexEntity);
                                        setListAdapter(getAdapter());
                                        break;
                                }
                            }
                        };
                        
                        adbuilder.setPositiveButton(getString(R.string.renameDiaog_okButtonText), listener);
                        adbuilder.setNegativeButton(getString(R.string.renameDiaog_cancelButtonText), null);

                        adbuilder.show();
                    }
                });

                adbuilder.show();
                
                return true;
            }
        });
        
        ((RelativeLayout) findViewById(R.id.emptyLayout)).setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent regexIntent = new Intent(context, RegexActivity.class);
                regexIntent.putExtra(RegexActivity.REGEX_ID_EXTRA_KEY, -1);
                context.startActivity(regexIntent);
            }
        });
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.favorite_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.favoriteMenu_menuItem_newRegex:
            {
                Intent regexIntent = new Intent(context, RegexActivity.class);
                regexIntent.putExtra(RegexActivity.REGEX_ID_EXTRA_KEY, -1);
                context.startActivity(regexIntent);
                return true;
            }
                
            case R.id.favoriteMenu_menuItem_rateApp:
            {
                try
                {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getString(R.string.general_applicationPackage))));
                }
                catch (Exception ex)
                {
                    String url = "https://play.google.com/store/apps/details?id=" + getString(R.string.general_applicationPackage);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                }
                return true;
            }
                
            case R.id.favoriteMenu_menuItem_buyBook:
            {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(getString(R.string.general_buyBook_url)));
                startActivity(browserIntent);
                return true;
            }
                
            default:
            {
                return super.onOptionsItemSelected(item);
            }
        }
    }
    
    
    private SimpleAdapter getAdapter()
    {
        List<RegexEntity> regexes = Arrays.asList(DbOpenHelper.getInstance(getApplicationContext()).getAllRegexes());
        
        return new SimpleAdapter(this, regexes, android.R.layout.simple_list_item_2,
                new String[] { RegexEntity.KEY_ALIAS, RegexEntity.KEY_REGEX }, 
                new int[] { android.R.id.text1, android.R.id.text2 })
                {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) 
                    {
                        View view = super.getView(position, convertView, parent);
                        TextView regex = (TextView) view.findViewById(android.R.id.text2);
                        String plain = regex.getText().toString();
                        String html = highlighter.highlight(plain);
                        Spannable spannable = new SpannableString(plain);
                        SpansHelper.fromHtml(html, spannable);
                        regex.setSingleLine(true);
                        regex.setText(spannable);
                        regex.setEllipsize(TruncateAt.END);
                        
                        return view;
                    }
                };
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        
        this.setListAdapter(getAdapter());
    }
}
