package com.andkorsh.regexpal.activities;

import java.util.concurrent.Callable;

import com.andkorsh.regexpal.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;

public class SettingsHelper
{
    private boolean caseInsensitive, caseInsensitiveNew;
    private boolean dotMathesAll, dotMathesAllNew;
    private boolean multiline, multilineNew;

    public SettingsHelper(Context context, boolean caseInsensitive, 
            boolean dotMathesAll, boolean multiline)
    {
        this.caseInsensitive = caseInsensitive;
        this.dotMathesAll = dotMathesAll;
        this.multiline = multiline;
    }

    public boolean isCaseInsensitive()
    {
        return caseInsensitive;
    }

    public boolean isDotMathesAll()
    {
        return dotMathesAll;
    }

    public boolean isMultiline()
    {
        return multiline;
    }

    public void showSettingsDialog(final Context context,
            final Callable<Void> onClick)
    {
        AlertDialog.Builder adbuilder = new AlertDialog.Builder(context);
        CharSequence items[] = new CharSequence[]
            { context.getString(R.string.settingsDialog_caseInsensitiveCheckbox_text),
                    context.getString(R.string.settingsDialog_dotMatchesAllCheckbox_text),
                    context.getString(R.string.settingsDialog_multilineCheckbox_text) };

        caseInsensitiveNew = caseInsensitive;
        dotMathesAllNew = dotMathesAll;
        multilineNew = multiline;

        adbuilder.setMultiChoiceItems(items, new boolean[]
            { caseInsensitive, dotMathesAll, multiline },
                new OnMultiChoiceClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which,
                            boolean isChecked)
                    {
                        switch (which)
                        {
                            case 0:
                                caseInsensitiveNew = isChecked;
                                break;

                            case 1:
                                dotMathesAllNew = isChecked;
                                break;

                            case 2:
                                multilineNew = isChecked;
                                break;

                            default:
                                break;
                        }
                    }
                });

        adbuilder.setTitle(context.getString(R.string.settingsDialog_title));
        adbuilder.setPositiveButton(
                context.getString(R.string.settingsDialog_applyButton_text),
                new OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (caseInsensitive != caseInsensitiveNew
                            || dotMathesAll != dotMathesAllNew
                            || multiline != multilineNew)
                        {
                            caseInsensitive = caseInsensitiveNew;
                            dotMathesAll = dotMathesAllNew;
                            multiline = multilineNew;
    
                            if (onClick != null)
                            {
                                try
                                {
                                    onClick.call();
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });

        adbuilder.show();
    }
}
