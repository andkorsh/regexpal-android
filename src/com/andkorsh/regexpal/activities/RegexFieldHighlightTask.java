package com.andkorsh.regexpal.activities;

import com.andkorsh.regexpal.helpers.RegexHighlighter;
import com.andkorsh.regexpal.helpers.SpansHelper;

import android.os.AsyncTask;
import android.text.Editable;

public class RegexFieldHighlightTask
{
    private class HighlightTask extends AsyncTask<Void, Void, String>
    {

        private Editable regexField;

        private HighlightTask(Editable regexField)
        {
            this.regexField = regexField;
        }

        @Override
        protected String doInBackground(Void... arg0)
        {

            String html = RegexHighlighter.getInstance().highlight(
                    regexField.toString());
            
            return html;
        }

        @Override
        protected void onPostExecute(String result)
        {
            if (isCancelled())
            {
                return;
            }

            SpansHelper.fromHtml(result, regexField);
        }
    }

    private static RegexFieldHighlightTask instance = null;
    private HighlightTask task = null;

    private RegexFieldHighlightTask()
    {
    }

    public static RegexFieldHighlightTask getInstance()
    {
        if (instance == null)
        {
            instance = new RegexFieldHighlightTask();
        }

        return instance;
    }

    public void highlight(Editable regexField)
    {
        if (task != null)
        {
            task.cancel(true);
        }

        task = new HighlightTask(regexField);
        task.execute();
    }
}
